import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";
import "./register.css";
import { END_POINT } from "../../constants/Constants";

function Register() {
  const username = useRef();
  const email = useRef();
  const password = useRef();
  const passwordAgain = useRef();
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (passwordAgain.current.value !== password.current.value) {
      passwordAgain.current.setCustomValidity("Passwords don't match!");
    } else {
      const user = {
        username: username.current.value,
        email: email.current.value,
        password: password.current.value,
      };
      try {
        await axios.post(`${END_POINT}/auth/register`, user);
        history.push("/login");
      } catch (err) {
        console.log(err);
      }
    }
  };

  return (
    <div className="register">
      <div className="registerWrapper">
        <div className="registerLeft">
          <h3 className="registerLogo">FSociety</h3>
          <span className="registerDesc">
            Connect with friends and the world around you on FSociety.
          </span>
        </div>
        <form className="registerRight" onSubmit={handleSubmit}>
          <div className="registerBox">
            <input
              placeholder="Username"
              className="registerInput"
              type="text"
              required
              ref={username}
            />
            <input
              placeholder="Email"
              className="registerInput"
              type="email"
              required
              ref={email}
            />
            <input
              placeholder="Password"
              className="registerInput"
              type="password"
              required
              ref={password}
              minLength="6"
            />
            <input
              placeholder="Password Again"
              className="registerInput"
              type="password"
              required
              ref={passwordAgain}
              minLength="6"
            />
            <button className="registerButton" type="submit">
              Sign Up
            </button>
            <button className="registerRegisterButton">Log into Account</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Register;
