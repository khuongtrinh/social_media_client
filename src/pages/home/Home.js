import Feed from "../../components/feed/Feed";
import LeftBar from "../../components/leftbar/LeftBar";
import RightBar from "../../components/rightbar/RightBar";
import TopBar from "../../components/topbar/TopBar";
import "./home.css";

function Home() {
  return (
    <>
      <TopBar />
      <div className="homeContainer">
        <LeftBar />
        <Feed username="john" />
        <RightBar profile />
      </div>
    </>
  );
}

export default Home;
