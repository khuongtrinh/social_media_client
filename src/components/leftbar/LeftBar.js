import {
  Bookmark,
  Chat,
  Event,
  Group,
  HelpOutline,
  PlayCircleFilledOutlined,
  RssFeed,
  School,
  WorkOutline,
} from "@mui/icons-material";
import "./leftbar.css";
import { Users } from "../../dummyData";
import CloseFriend from "../closeFriend/CloseFriend";

function LeftBar() {
  return (
    <div className="leftBar">
      <div className="leftBarWrapper">
        <ul className="leftBarList">
          <li className="leftBarItem">
            <RssFeed className="leftBarIcon" />
            <span className="leftBarItemText">Feed</span>
          </li>
          <li className="leftBarItem">
            <Chat className="leftBarIcon" />
            <span className="leftBarItemText">Chats</span>
          </li>
          <li className="leftBarItem">
            <PlayCircleFilledOutlined className="leftBarIcon" />
            <span className="leftBarItemText">Videos</span>
          </li>
          <li className="leftBarItem">
            <Group className="leftBarIcon" />
            <span className="leftBarItemText">Groups</span>
          </li>
          <li className="leftBarItem">
            <Bookmark className="leftBarIcon" />
            <span className="leftBarItemText">Bookmarks</span>
          </li>
          <li className="leftBarItem">
            <HelpOutline className="leftBarIcon" />
            <span className="leftBarItemText">Questions</span>
          </li>
          <li className="leftBarItem">
            <WorkOutline className="leftBarIcon" />
            <span className="leftBarItemText">Jobs</span>
          </li>
          <li className="leftBarItem">
            <Event className="leftBarIcon" />
            <span className="leftBarItemText">Events</span>
          </li>
          <li className="leftBarItem">
            <School className="leftBarIcon" />
            <span className="leftBarItemText">Courses</span>
          </li>
        </ul>
        <button className="leftBarButton">Show More</button>
        <hr className="leftBarHr" />
        <ul className="leftBarFriendList">
          {Users.map((u) => (
            <CloseFriend key={u.id} user={u} />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default LeftBar;
