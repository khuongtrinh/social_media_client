import "./closefriend.css";

function CloseFriend({ user }) {
  const PF = process.env.REACT_APP_PUBLIC_FOLDER;
  return (
    <li className="leftBarFriend">
      <img src={PF + user.profilePicture} alt="" className="leftBarFriendImg" />
      <span className="leftBarFriendName">{user.username}</span>
    </li>
  );
}

export default CloseFriend;
